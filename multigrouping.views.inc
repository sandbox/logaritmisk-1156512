<?php

function template_preprocess_multigrouping_view_multigrouping(&$vars) {
  $view = $vars['view'];
  $rows = $vars['rows'];

  $vars['classes_array'] = array();
  $vars['classes'] = array();
  // Set up striping values.
  $count = 0;
  $max = count($rows);
  foreach ($rows as $id => $row) {
    $count++;
    $vars['classes'][$id][] = 'views-row';
    $vars['classes'][$id][] = 'views-row-' . $count;
    $vars['classes'][$id][] = 'views-row-' . ($count % 2 ? 'odd' : 'even');
    if ($count == 1) {
      $vars['classes'][$id][] = 'views-row-first';
    }
    if ($count == $max) {
      $vars['classes'][$id][] = 'views-row-last';
    }

    if ($row_class = $view->style_plugin->get_row_class($id)) {
      $vars['classes'][$id][] = $row_class;
    }

    // Flatten the classes to a string for each row for the template file.
    $vars['classes_array'][$id] = implode(' ', $vars['classes'][$id]);
  }
  
  if (!empty($vars['title'])) {
    if ($vars['options']['grouping_type'] !== '0') {
      if ($vars['options']['grouping_type'] === '') {
        $vars['options']['grouping_type'] = 'div';
      }
      $class = trim($vars['options']['grouping_class']);
      if (strlen($class)) {
        $class = ' class="'. $class . '"';
      }
      $vars['title'] = '<' . $vars['options']['grouping_type'] . $class . '>' . $vars['title'] . '</' . $vars['options']['grouping_type'] . '>';
    }
  }
}

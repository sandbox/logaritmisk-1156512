<?php
/**
 * @file
 */


/**
 *
 */
class multigrouping_plugin_style_multigroup extends views_plugin_style {

  /**
   *
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['grouping_type'] = array(
      'default' => array('')
    );
    $options['grouping_class'] = array(
      'default' => array('')
    );
    $options['grouping_fields'] = array(
      'default' => array()
    );

    return $options;
  }

  /**
   *
   */
  function options_form(&$form, &$form_state) {
    if ($this->uses_fields() && $this->definition['uses grouping']) {
      $options = array('' => t('- None -')) + $this->display->handler->get_field_labels();
      $elements = variable_get('views_field_rewrite_elements', array(
        '' => t('- Use default -'),
        '0' => t('- None -'),
        'div' => t('DIV'),
        'span' => t('SPAN'),
        'h1' => t('H1'),
        'h2' => t('H2'),
        'h3' => t('H3'),
        'h4' => t('H4'),
        'h5' => t('H5'),
        'h6' => t('H6'),
        'p' => t('P'),
        'strong' => t('STRONG'),
        'em' => t('EM'),
      ));

      if (count($options) > 1) {
        $i = 0;

        foreach ($this->options['grouping_fields'] as $grouping) {
          $form['grouping_fields'][$i++] = array(
            '#type' => 'select',
            '#title' => t('Field #!no', array('!no' => $i)),
            '#options' => $options,
            '#default_value' => $grouping,
            '#description' => t('You may optionally specify a field by which to group the records. Leave blank to not group.'),
          );
        }

        $form['grouping_fields'][$i++] = array(
          '#type' => 'select',
          '#title' => t('Field #!no', array('!no' => $i)),
          '#options' => $options,
          '#default_value' => '',
          '#description' => t('You may optionally specify a field by which to group the records. Leave blank to not group.'),
        );
        $form['grouping_fields'][$i++] = array(
          '#type' => 'select',
          '#title' => t('Field #!no', array('!no' => $i)),
          '#options' => $options,
          '#default_value' => '',
          '#description' => t('You may optionally specify a field by which to group the records. Leave blank to not group.'),
        );
      }

      $form['grouping_type_enable'] = array(
        '#type' => 'checkbox',
        '#title' => t('Customize wrapping HTML'),
        '#default_value' => !empty($this->options['grouping_type']) || (string) $this->options['grouping_type'] == '0' || !empty($this->options['grouping_class']) || (string) $this->options['grouping_class'] == '0',
      );
      $form['grouping_type'] = array(
        '#type' => 'select',
        '#title' => t('HTML element'),
        '#options' => $elements,
        '#default_value' => $this->options['grouping_type'],
        '#dependency' => array(
          'edit-style-options-grouping-type-enable' => array(1),
        ),
      );
      $form['grouping_class_enable'] = array(
        '#type' => 'checkbox',
        '#title' => t('Create a CSS class'),
        '#default_value' => !empty($this->options['grouping_class']) || (string) $this->options['grouping_class'] == '0',
        '#dependency' => array(
          'edit-style-options-grouping-type-enable' => array(1),
        ),
      );
      $form['grouping_class'] = array(
        '#type' => 'textfield',
        '#title' => t('CSS class'),
        '#default_value' => $this->options['grouping_class'],
        '#dependency' => array(
          'edit-style-options-grouping-class-enable' => array(1),
          'edit-style-options-grouping-type-enable' => array(1),
        ),
        '#dependency_count' => 2,
      );
    }

    if ($this->uses_row_class()) {
      $form['row_class'] = array(
        '#title' => t('Row class'),
        '#description' => t('The class to provide on each row.'),
        '#type' => 'textfield',
        '#default_value' => $this->options['row_class'],
      );

      if ($this->uses_fields()) {
        $form['row_class']['#description'] .= ' ' . t('You may use field tokens from as per the "Replacement patterns" used in "Rewrite the output of this field" for all fields.');
      }
    }
  }

  /**
   *
   */
  function options_submit(&$form, &$form_state) {
    $form_state['values']['style_options']['grouping_fields'] = array_filter($form_state['values']['style_options']['grouping_fields'], 'strlen');
  }

  /**
   *
   */
  function render() {
    if ($this->uses_row_plugin() && empty($this->row_plugin)) {
      debug('views_plugin_style_default: Missing row plugin');
      return;
    }

    $sets = $this->render_grouping($this->view->result, $this->options['grouping_fields']);

    $output = '';

    $set = $sets;

    for ($i = 1; $i < count($this->options['grouping_fields']); $i++) {
      $obj = array();
      foreach ($set as $l => $r) {
        foreach ($r as $t => $o) {
          $obj[$l . $t] = $o;
          $l = '';
        }
      }
      $set = $obj;
    }

    foreach ($set as $title => $records) {
      if ($this->uses_row_plugin()) {
        $rows = array();
        foreach ($records as $row_index => $row) {
          $this->view->row_index = $row_index;
          $rows[$row_index] = $this->row_plugin->render($row);
        }
      }
      else {
        $rows = $records;
      }

      $output .= theme($this->theme_functions(),
        array(
          'view' => $this->view,
          'options' => $this->options,
          'rows' => $rows,
          'title' => $title
        )
      );
    }

    unset($this->view->row_index);

    return $output;
  }

  /**
   *
   */
  function render_grouping($records, $groupings = array(), $group_rendered = NULL) {
    $this->render_fields($this->view->result);
    $sets = array();

    if ($groupings) {
      foreach ($records as $index => $row) {
        $set = &$sets;
        foreach ($groupings as $field) {
          $grouping = '';
          if (isset($this->view->field[$field])) {
            $grouping = trim($this->render_grouping_field($index, $field));
          }
          if (!isset($set[$grouping])) {
            $set[$grouping] = array();
          }
          $set = &$set[$grouping];
        }
        $set[$index] = $row;
      }
    }
    else {
      $sets[''] = $records;
    }

    return $sets;
  }

  /**
   *
   */
  function render_grouping_field($index, $field) {
    $options = array();

    foreach ($this->view->field as $id => $field_instance) {
      $options[$id] = $field_instance->options;

      if ($id === $field) {
        $field_instance->options['exclude'] = FALSE;
        $field_instance->options['element_wrapper_type'] = '0';
      }
      else {
        $field_instance->options['exclude'] = TRUE;
      }
    }

    $this->view->row_index = $index;

    $output = theme($this->row_plugin->theme_functions(),
      array(
        'view' => $this->view,
        'options' => $this->row_plugin->options,
        'row' =>  $this->view->result[$index],
        'field_alias' => isset($this->row_plugin->field_alias) ? $this->row_plugin->field_alias : '',
      ));

    foreach ($this->view->field as $id => $field_instance) {
      $field_instance->options = $options[$id];
    }

    return $output;
  }

}
